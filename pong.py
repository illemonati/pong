# import keras
import tensorflow as tf
keras = tf.keras
import gym
import numpy as np
import random
from collections import deque


class Agent:
    def __init__(self, state_shape, action_size):
        self.action_size = action_size
        self.state_size = state_shape
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.epsilon_min = 0.001
        self.alpha = 0.01
        self.gamma = 0.8
        self.model = self.make_model()
        self.batch_size = 32
        self.memory = deque(maxlen=300)

    def make_model(self):
        # inputs = keras.Input(shape=self.state_size)
        # layer1 = keras.layers.Dense(64, activation='relu')(inputs)
        # layer2 = keras.layers.Dense(64, activation='relu')(layer1)
        # layer3 = keras.layers.Dense(64, activation='relu')(layer2)
        # output = keras.layers.Dense(self.action_size, activation='softmax')(layer3)
        # model = keras.Model(inputs=inputs, output=output)
        model = keras.Sequential([
            keras.layers.Flatten(input_shape=self.state_size),
            keras.layers.Dense(128, activation='relu'),
            keras.layers.Dense(256, activation='relu'),
            keras.layers.Dense(512, activation='relu'),
            keras.layers.Dense(6, activation='softmax')
        ])
        model.compile(optimizer=keras.optimizers.Adam(lr=self.alpha),
                      loss='categorical_crossentropy',
                      metrics=['acc'])
        model.summary()
        return model

    def act(self, state):
        if random.random() < self.epsilon:
            return random.randrange(self.action_size)
        else:
            state = np.expand_dims(state, axis=0)
            return np.argmax(self.model.predict(state)[0])

    def load(self, path):
        self.model = keras.models.load_model(path)


    def remember(self, state, action, reward, next_state, done):
        self.memory.append((self, state, action, reward, next_state, done))

    def replay(self):
        batch = random.sample(self.memory, self.batch_size)
        for self, state, action, reward, next_state, done in batch:
            state = np.expand_dims(state, axis=0)
            next_state = np.expand_dims(next_state, axis=0)
            if done:
                target = reward
            else:
                target = reward + self.gamma * np.argmax(self.model.predict(next_state)[0])
            future_target = self.model.predict(state)
            future_target[0][action] = target
            self.model.fit(state, future_target, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
        self.model.save("model.h5")


if __name__ == '__main__':
    env = gym.make("Pong-v0")
    done = False
    agent = Agent((210, 160, 3), 6)
    agent.load('model.h5')
    for episode in range(1000):
        tr = 0
        state = env.reset()
        done = False
        for _ in range(500):
            # env.render()
            action = agent.act(state)
            next_state, reward, done, info = env.step(action)
            tr += reward
            agent.remember(state, action, reward, next_state, done)
            if done:
                break
        print(f"ep : {episode} | reward = {tr}")
        agent.replay()



