# import keras
import tensorflow as tf
keras = tf.keras
import gym
import numpy as np

model = keras.models.load_model('model.h5')

if __name__ == '__main__':
    env = gym.make("Pong-v0")
    done = False
    state = env.reset()
    done = False
    while not done:
        env.render()
        state = np.expand_dims(state, axis=0)
        action = np.argmax(model.predict(state))
        state, reward, done, info = env.step(action)
